# Diff Revision Activity Metrics

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/code-metrics/ci-cd-extension-scc)

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

- **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

- **Publish Date**: 2022-11-18

- **GitLab Version Released On**: 15.4

- **Tested On**: 
  - GitLab Docker-Executor Runner

- **References and Featured In**:

## Feature List

- Configuration (set these in the projects using this extension):
  - **CODE_COUNTING_TOKEN**: Optional. PAT or Group or Project Access token that has `read_api` and `read_repository` and at least the role `Reporter`. Must either provide this AND CODE_COUNTING_USER or nothing for both. If these are blank, pipeline will use the runner token instead. This should not be coded into the pipeline, but rather into masked CI/CD variables so it is not in the code and can be masked in logs.
  - **CODE_COUNTING_USER**: Optional. User id of the PAT or Group / Project Access Token used above. Must either provide this AND CODE_COUNTING_TOKEN or nothing for both. If these are blank, pipeline will use the runner token instead.
  - **top_group_to_enumerate**: Optional. Will enumerate every project under this given group on the same GitLab instance as this pipeline runs. If left blank something must be provided for list_of_repos.
  - **list_of_repos**: Optional. Will count the exact groups in this list.  If left blank something must be provided for top_group_to_enumerate. Overrides top_group_to_enumerate if both are provided. Should include the ".git" extension. Each project can have authentication information in the URL such as https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/guided-explorations/agitrepo.git
  - **ignore_file_types_for_sparse_checkout**: List of (usually binary) files to ignore during sparse checkout to save time before counting starts. Defaults to `*.*\n!**/*.idx\n!**/*.mp4\n!**/*.gif\n!**/*.pdf\n!**/*.png\n!**/*.jpg\n!**/*.jpeg\n!**/*.eps\n!**/*.md\n` Changes must be formatted the same way as the default.

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of CI/CD extensions like this one, the working pattern may be it's use in another Guided Exploration.

